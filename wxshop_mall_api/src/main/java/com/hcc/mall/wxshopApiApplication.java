package com.hcc.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


//@EnableWebMvc
//@EnableSwagger2
//@ComponentScan("com.hcc.*")
@SpringBootApplication
public class wxshopApiApplication {
  public static void main(String[] args) {
    SpringApplication.run(wxshopApiApplication.class, args);
  }

}
