package com.hcc.mall.dao;

import com.hcc.common.dao.BaseDao;
import com.hcc.mall.entity.UserLevelVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商城_用户级别
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @gitee https://gitee.com/fuyang_lipengjun/platform
 * @date 2017-08-11 09:16:47
 */
@Mapper
public interface ApiUserLevelMapper extends BaseDao<UserLevelVo> {

}
