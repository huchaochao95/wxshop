package com.hcc.mall.dao;

import com.hcc.common.dao.BaseDao;
import com.hcc.mall.entity.SpecificationVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 规格表
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @gitee https://gitee.com/fuyang_lipengjun/platform
 * @date 2017-08-11 09:16:46
 */
@Mapper
public interface ApiSpecificationMapper extends BaseDao<SpecificationVo> {

}
