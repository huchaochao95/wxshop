package com.hcc.mall.dao;

import com.hcc.common.dao.BaseDao;
import com.hcc.mall.entity.CommentPictureVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lipengjun
 * @email 939961241@qq.com
 * @gitee https://gitee.com/fuyang_lipengjun/platform
 * @date 2017-08-11 09:14:26
 */
@Mapper
public interface ApiCommentPictureMapper extends BaseDao<CommentPictureVo> {

}
