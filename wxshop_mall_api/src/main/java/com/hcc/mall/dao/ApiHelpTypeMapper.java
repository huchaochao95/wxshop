package com.hcc.mall.dao;

import com.hcc.common.dao.BaseDao;
import com.hcc.mall.entity.HelpTypeVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @gitee https://gitee.com/fuyang_lipengjun/platform
 * @date 2017-12-02 10:04:20
 */
@Mapper
public interface ApiHelpTypeMapper extends BaseDao<HelpTypeVo> {

}
